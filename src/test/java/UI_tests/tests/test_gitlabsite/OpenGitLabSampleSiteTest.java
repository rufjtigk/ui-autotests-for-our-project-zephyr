package UI_tests.tests.test_gitlabsite;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class OpenGitLabSampleSiteTest {

    public WebDriver wd;



    @Test
    public void openSampleSiteTest() {
        final String USERNAME = "asdfzxcv3";
        final String AUTOMATE_KEY = "9kurYXVbszrh6RGUYxEh";
        final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

        DesiredCapabilities caps = new DesiredCapabilities();

        caps.setCapability("os", "Windows");
        caps.setCapability("os_version", "10");
        caps.setCapability("browser", "Chrome");
        caps.setCapability("browser_version", "80");
        caps.setCapability("name", "asdfzxcv3's test from gitlab");

        try {
            wd = new RemoteWebDriver(new URL(URL), caps);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        wd.navigate().to("https://rufjtigk.gitlab.io/our-project-zephyr/");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(wd.getTitle(), "Home");
        wd.quit();
    }
}
