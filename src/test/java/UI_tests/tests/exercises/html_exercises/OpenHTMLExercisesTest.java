package UI_tests.tests.exercises.html_exercises;

import UI_tests.tests.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

public class OpenHTMLExercisesTest extends TestBase {

    @Test(description = "The HTML Exercises page can be opened")
    public void testOpenHTMLExercises() {
        app.goTo().htmlExercisesPage();
        Assert.assertTrue(app.htmlExercisesPage().isOpened(), "The HTML Exercises page is not opened");
    }
}
