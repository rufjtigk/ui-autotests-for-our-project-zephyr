package UI_tests.tests.exercises.css_exercises;

import UI_tests.tests.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

public class OpenCSSExercisesTest extends TestBase {

    @Test(description = "The CSS Exercises page can be opened", groups = {"smoke"})
    public void testOpenCSSExercisesTest() {
        app.goTo().cssExercisesPage();
        Assert.assertTrue(app.cssExercisesPage().isOpened(), "The CSS Exercises page is not opened");
    }
}
