package UI_tests.tests.tutorials.html_tutorial;

import UI_tests.tests.TestBase;
import org.testng.ITestResult;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class OpenHTMLTutorialTest extends TestBase {

    @Test(description = "HTML tutorial can be opened")
    public void testHTMLTutorialCanBeOpened() {
        app.goTo().mainPage();
        app.mainPage().openHTMLTutorial();
        assertTrue(app.htmlTutorial().isTutorialsMainPageOpened(), "The main page of the HTML Tutorial is not opened!");
        assertTrue(false);
    }
}
