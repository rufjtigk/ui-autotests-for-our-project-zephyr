package UI_tests.tests.tutorials.css_tutorial;

import UI_tests.tests.TestBase;
import org.testng.ITestResult;
import org.testng.annotations.*;

import static org.testng.Assert.assertTrue;

public class OpenCSSTutorialTest extends TestBase {

    @Test(description = "CSS Tutorial can be opened", groups = {"smoke"})
    public void testCSSTutorialCanBeOpened() {
        app.goTo().mainPage();
        app.mainPage().openCSSTutorial();
        assertTrue(app.cssTutorial().isTutorialsMainPageOpened(),
                "The main page of the CSS Tutorial is not opened!");
    }
}
