package UI_tests.tests;

import UI_tests.app.ApplicationManager;
import UI_tests.app.TestNGTestListener;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.BrowserType;
import org.testng.ITestContext;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;

@Listeners(TestNGTestListener.class)
public class TestBase {

    protected static ApplicationManager app;

    @BeforeSuite(alwaysRun = true)
    public void setUp(ITestContext context) throws Exception {
        app = new ApplicationManager(System.getProperty("browser", BrowserType.CHROME));
        app.init();
        context.setAttribute("app", app);
    }

    @AfterSuite(alwaysRun = true)
    public void tearDown() {
        app.stop();
        app = null;
    }
}
