package UI_tests.tests;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.io.File;

public class Sandbox {

    //projectId = 10000, projectKey = OP

    @Test
    public void testGetProjectID() {
        HttpResponse<JsonNode> response = Unirest.get("https://rufjtigk1234.atlassian.net/rest/api/3/project/OP")
                .basicAuth("rufjtigk@gmail.com", "zZOHzUpeanYhHDkxBfRTE548")
                .header("Accept", "application/json")
                .asJson();

        System.out.println(response.getBody().getObject().get("id"));
        System.out.println(response.getBody());
    }

    @Test
    public void testGetAnIssue() {
        HttpResponse<JsonNode> response = Unirest.get("https://rufjtigk1234.atlassian.net/rest/api/3/issue/OP-12")
                .basicAuth("rufjtigk@gmail.com", "zZOHzUpeanYhHDkxBfRTE548")
                .header("Accept", "application/json")
                .asJson();

        System.out.println(response.getBody());
    }
}
