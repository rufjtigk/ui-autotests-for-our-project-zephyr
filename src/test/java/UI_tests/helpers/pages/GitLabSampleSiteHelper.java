package UI_tests.helpers.pages;

import UI_tests.app.ApplicationManager;
import UI_tests.helpers.HelperBase;

public class GitLabSampleSiteHelper extends HelperBase {
    public GitLabSampleSiteHelper(ApplicationManager app) {
        super(app);
    }

    public boolean isOpened() {
        return wd.getTitle().equals("Home");
    }
}
