package UI_tests.helpers.pages;

import UI_tests.app.ApplicationManager;
import UI_tests.helpers.HelperBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static org.openqa.selenium.support.ui.ExpectedConditions.titleIs;

public class MainPageHelper extends HelperBase {

    @FindBy(css = "#mySidenav a[href*=\"/html/default\"]")
    public WebElement htmlTutorialNavigationButton;

    @FindBy(css = "#mySidenav a[href*=\"/css/default\"]")
    public WebElement cssTutorialNavigationButton;

    public MainPageHelper(ApplicationManager app) {
        super(app);
        PageFactory.initElements(wd, this);
    }

    public void openCSSTutorial() {
        cssTutorialNavigationButton.click();
        wait.until(wd -> titleIs("CSS Tutorial"));
    }

    public void openHTMLTutorial() {
        htmlTutorialNavigationButton.click();
        wait.until(wd -> titleIs("HTML Tutorial"));
    }
}
