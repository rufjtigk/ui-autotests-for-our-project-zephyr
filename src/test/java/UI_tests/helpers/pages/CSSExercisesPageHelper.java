package UI_tests.helpers.pages;

import UI_tests.app.ApplicationManager;
import UI_tests.helpers.HelperBase;

public class CSSExercisesPageHelper extends HelperBase {
    public CSSExercisesPageHelper(ApplicationManager app) {
        super(app);
    }

    public boolean isOpened() {
        return wd.getTitle().equals("CSS Exercises");
    }
}
