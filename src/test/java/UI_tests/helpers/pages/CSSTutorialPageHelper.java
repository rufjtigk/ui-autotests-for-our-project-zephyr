package UI_tests.helpers.pages;

import UI_tests.app.ApplicationManager;
import UI_tests.helpers.HelperBase;

public class CSSTutorialPageHelper extends HelperBase {
    public CSSTutorialPageHelper(ApplicationManager app) {
        super(app);
    }

    public boolean isTutorialsMainPageOpened() {
        return wd.getTitle().equals("CSS Tutorial");
    }
}
