package UI_tests.helpers.pages;

import UI_tests.app.ApplicationManager;
import UI_tests.helpers.HelperBase;

public class HTMLExercisesPageHelper extends HelperBase {
    public HTMLExercisesPageHelper(ApplicationManager app) {
        super(app);
    }

    public boolean isOpened() {
        return wd.getTitle().equals("HTML Exercises");
    }
}
