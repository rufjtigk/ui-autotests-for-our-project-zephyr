package UI_tests.helpers;

import UI_tests.app.ApplicationManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NavigationHelper extends HelperBase {

    private final String baseUrl;

    @FindBy(css = "#navbtn_exercises")
    WebElement exercisesNavigationButton;

    @FindBy(css = "#nav_exercises a[href*=\"/html_exercises\"]")
    WebElement htmlExercisesNavigationButton;

    @FindBy(css = ".w3-col a.w3schools-logo")
    WebElement logotype;

    @FindBy(css = "#nav_exercises a[href*=\"/css_exercises\"]")
    WebElement cssExercisesNavigationButton;

    public NavigationHelper(ApplicationManager app) {
        super(app);
        baseUrl = app.getProperties().getProperty("web.baseUrl");
        PageFactory.initElements(wd, this);
    }

    public void mainPage() {
        if (isElementPresent(logotype)) {
            logotype.click();
        } else {
            wd.navigate().to(baseUrl);
        }
    }

    public void htmlExercisesPage() {
        mainPage();
        exercisesNavigationButton.click();
        htmlExercisesNavigationButton.click();
    }

    public void cssExercisesPage() {
        mainPage();
        exercisesNavigationButton.click();
        cssExercisesNavigationButton.click();
    }

    public void sampleGitLabSite() {
        wd.navigate().to("https://rufjtigk.gitlab.io/our-project-zephyr/");
    }
}
